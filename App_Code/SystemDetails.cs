﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;

/// <summary>
/// Summary description for SystemDetails
/// </summary>
public class SystemDetails
{
    public SystemDetails()
    {
        //
        // TODO: Add constructor logic here

        //
    }

    public static PhysicalAddress GetMAC()
    {
        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            // Only consider Ethernet network interfaces
            if (nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 &&
                nic.OperationalStatus == OperationalStatus.Up)
            {
                return nic.GetPhysicalAddress();
            }
        }
        return null;
    }

    public static OperatingSystem GetOS()
    {
        OperatingSystem os = Environment.OSVersion;
        return os;
    }

    public static string SystemName()
    {
        return Environment.MachineName;
    }


}