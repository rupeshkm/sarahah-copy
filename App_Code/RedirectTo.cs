﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using PAL;
using System.Windows;
/// <summary>
/// Summary description for RedirectTo
/// </summary>
public class RedirectTo

{
    public RedirectTo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int profile = 1;
    public static void Redirect(string Page,string QueryString)
    {
        switch (Page)
        {
            case "message":
                HttpContext.Current.Response.Redirect("~/Master/Welcome/Messages.aspx?"+QueryString);
                break;
            case "newmsg":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/NewMessage.aspx?"+QueryString);
                    break;
                }
        }
    }
    public static void Redirect(string Page)
    {
        switch(Page){
           case "profile":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/Profile.aspx");
                    break;
                }
            case "home":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/Home.aspx");
                    break;
                }
           
            case "logout":
                {
                    HttpContext.Current.Session.Remove("user");
                    HttpContext.Current.Session.RemoveAll();
                    Information.total_active--;
                    HttpContext.Current.Response.Redirect("~/Master/Get_Started/logIn.aspx");
                   
                    break;
                }
            case "forgot_password":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Get_Started/ForgotPassword.aspx");
                    break;
                }
           

            case "login":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Get_Started/login.aspx");
                    break;
                }
            case "settings":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Settings/Default.aspx");
                    break;
                }

            case "password":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Settings/Password.aspx");
                    break;
                }
            case "remove":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Settings/RemoveAccount.aspx");
                    break;
                }
            case "messages":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/Messages.aspx");
                    break;
                }
            case "signup":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Get_Started/Default.aspx");
                    break;
                }
            case "fav":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/Favorites.aspx");
                    break;
                }
            case "sent":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/Sent.aspx");
                    break;
                }
            case "newmsg":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/NewMessage.aspx");
                    break;
                }
            case "srch":
                {
                    HttpContext.Current.Response.Redirect("~/Master/Welcome/SearchResults.aspx");
                    break;
                }

        }

                
    }

   
    
    public static void UpdateSession(string email)
    {

        HttpContext.Current.Session.Remove("user");
        HttpContext.Current.Session.RemoveAll();
        HttpContext.Current.Session["user"] = email;
    }
    
}