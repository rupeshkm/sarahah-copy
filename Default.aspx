﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Shutterbuzz</title>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-7998368577300395",
    enable_page_level_ads: true
  });
</script>
    <meta property="og:url" content="http://www.shutterbuzz.in/Welcome/Profile.aspx" />
	
<meta property="og:title" content="www.shutterbuzz.in" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.shutterbuzz.in/assets/img/Picture1.png" />
	
	
<meta property="og:description" content="Send anonymous messages to me at www.shutterbuzz.in. Now you can also send back reply to the messages your recieved without revealing identity." />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css' />--%>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" />--%>
    <link rel="stylesheet" href="~/css/style.css" />
    <script src="../../Scripts/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="~/Content/bootstrap.css" rel="stylesheet" />

</head>
<body>
        <form id="form1" runat="server">
            <nav class="navbar navbar-inverse" style="background-color: #424242">
                <div class="container">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Shutterbuzz</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <%--<ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Page 1-1</a></li>
                                <li><a href="#">Page 1-2</a></li>
                                <li><a href="#">Page 1-3</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>--%>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <asp:LinkButton ID="lblSignUp" runat="server" OnClick="lblSignUp_Click"><span class="glyphicon glyphicon-user"></span>Sign Up</asp:LinkButton></li>

                            <li>
                                <asp:LinkButton ID="lblLogin" runat="server" OnClick="lblLogin_Click"><span class="glyphicon glyphicon-log-in"></span>Login</asp:LinkButton></li>

                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">


                <div class="row">
                    <div class="col-lg-12"><h1 class="media-heading" style="font-family: 'Monotype Corsiva';color:rebeccapurple"><b>Welcome to Shutterbuzz</b></h1><br /><h3 class="media-heading text-center text-info" style="font-family:Aparajita">Send Anonymous Text</h3></div>
                </div><br />
                <div class="row">
                    <div class="col-lg-12 well text-center"><b style="font-family:Consolas">Send anonyomus messages to your friends and co-workers.<br />Let them know their actual feedback.</b></div>
                    <div class="col-lg-12 well text-center text-danger"><b style="font-family:Consolas;">Send reply to the anonymous messages you received</b><br /><br /></div>
                </div><br /><br />
				
                <div class="row">
                    <%--<div class="col-lg-12 text-center" style="font-family:Consolas;font-size:larger;color:blue">Share on Facebook<img src="assets/img/Facebook.png" class="img-responsive" height="30" width="30" /></div>--%>
                    <a href="http://www.facebook.com/sharer/sharer.php?s=100&p[url]=
http://www.shutterbuzz.in">
                    <img src="../assets/img/share-on-facebook.png" onmouseover="Share" height="100" width="120  " class="img-responsive center-block"  /></a>
  
                </div>
               
                <div>
                </div>
            </div>
            <div class="footer" style="position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #efefef;
  text-align: center;">
                <div class="row">
                <div class="col-lg-6 text-center" style="font-family:Consolas;">Contact Us : <b style="color:cornflowerblue">support@shutterbuzz.in</b></div>
                <div class="col-lg-6 text-center" style="font-family:Consolas;"><asp:LinkButton ID="ltbnFB" runat="server" OnClick="ltbnFB_Click"><img src="../assets/img/Facebook.png" onmouseover="Share" height="25" width="25" class="img-responsive center-block"  /></a>
  </asp:LinkButton></div>
               
                </div>
                </div>
            </form>
    </div>
    
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../../Scripts/bootstrap.min.js"></script>
    <script src="../../Scripts/jquery-1.9.0.min.js"></script>
    <script src="js/index.js"></script>

</body>
</html>
