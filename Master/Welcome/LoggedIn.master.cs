﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class LoggedIn : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }
        
    }

    protected void lBtnSetting_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("settings");
    }

    protected void lblLogout_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("logout");
        
    }

    protected void lBtnProfile_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("profile");
    }

    protected void lBtnMsg_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("messages");
    }

    protected void lBtnHome_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("home");
    }

   public void temp()
    {
        RedirectTo.Redirect("newmsg");
    }

    

    

    protected void BtnSrch_ServerClick(object sender, EventArgs e)
    {
        PAL.Information info = new PAL.Information();
        
        Session["SearchUser"] = TxtSrch.Text;
        if(TxtSrch.Text != "")
        {

            RedirectTo.Redirect("srch");
        }

    }


    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Welcome/Replies.aspx?reply=recieved");

        //RedirectTo.Redirect("messages");
    }

    protected void ltbnFB_Click(object sender, EventArgs e)
    {
        Response.Redirect("https://www.facebook.com/shutterbuzz.in/");
    }
}
