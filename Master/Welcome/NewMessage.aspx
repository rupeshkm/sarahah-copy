﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/LoggedIn.master" AutoEventWireup="true" CodeFile="NewMessage.aspx.cs" Inherits="Master_Welcome_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <script type="text/javascript">
        function validate() {
            if (document.getElementById("<%=txtSendTo.ClientID%>").value == "") {
                alert("Please Enter Username");
                document.getElementById("<%=txtSendTo.ClientID%>").focus();
                console.debug("function called");
                return false;
            }
            return true;
        }
    </script>
    <div class="container">
        <div class="col-lg-6 col-lg-offset-3">
        <div class="row text-center">
            <div class="col-lg-12">
                <asp:Label ID="lblMsgStatus" runat="server" Text=""></asp:Label>
            </div>
        
            <div class="panel panel-success" >
                <div class="panel-heading">
                        <div class="input-group">
                            <asp:TextBox ID="txtSrch" runat="server" CssClass="form-control" placeholder="Search Username" />
                            <div class="input-group-btn">
                                <asp:LinkButton ID="LbtnSearch" runat="server" CssClass="btn btn-default" OnClick="LbtnSearch_Click"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                            </div>
                        </div>
                    
                    </div>
                <div class="panel-body">
                        <asp:TextBox ID="txtSendTo" runat="server" CssClass="form-control"></asp:TextBox><br />
                        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Height="300" placeholder="Leave a message" CssClass="form-control"></asp:TextBox>
                    </div>
                <div class="panel-footer text-center" style="background-color:white;border:none">
                    <asp:LinkButton ID="lBtn1" runat="server" CssClass="btn btn-success" OnClientClick= "return validate()" OnClick="lBtn1_Click"><strong> Send </strong><span class="glyphicon glyphicon-send"></span></asp:LinkButton>
                </div>
               </div>
            </div>
        </div>
       <%-- <a href="http://www.facebook.com/sharer/sharer.php?s=100&p[url]=
http://rupeshkumar93-001-site1.itempurl.com/Master/Welcome/NewMessage.aspx">Share on Facebook</a>--%>
    </div>
</asp:Content>

