﻿using System;
using BAL;
using PAL;
using System.Diagnostics;

public partial class Master_Welcome_Default : System.Web.UI.Page
{

    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {
        Debug.WriteLine("SessionUser " + Session["user_from"]);
        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
            return;
        }

        if (Session["username"] != null)
        {
            txtSendTo.Text = Session["username"].ToString();
            
            Session.Remove("username");
            

        }
        if(Session["user_from"] != null)
        {
            info.receive = Session["user_from"].ToString();
            //info.receive = (Request.QueryString["to"]);
            //info.uid = Request.QueryString["id"];

            //Debug.WriteLine("ReferenceNumber " + info.uid);
            txtSendTo.Text = info.receive;
            txtSendTo.Visible = false;
            txtSrch.Visible = false;
            LbtnSearch.Visible = false;

        }
    }

    protected void LbtnSearch_Click(object sender, EventArgs e)
    {
        info.receive = txtSrch.Text;
       
        object recieve = opr.CheckEmail1(info, "user");
        if(recieve != null)
        {  
            txtSendTo.Text = recieve.ToString();
        }
        else
        {
            lblMsgStatus.Text = "<b style='color:red'>User not available</b>";

        }
    }

    protected void lBtn1_Click(object sender, EventArgs e)
    {
        
        if (Session["user_from"] == null)
        {
            object username = opr.GetUsername(Session["user"].ToString());
            if (username != null)
            {
                info.username = username.ToString();
            }
            if (!String.IsNullOrEmpty(txtSendTo.Text))
            {
                info.receive = txtSendTo.Text;
                if (!info.receive.Contains(".shutterbuzz.in"))
                    info.receive += ".shutterbuzz.in";
                info.message = TextBox1.Text;

                int i = opr.SendMessage(info,"new");
                if (i > 0)
                {

                    lblMsgStatus.Text = "<b style='color:green'>Message sent successfully</b>";
                    //RedirectTo.Redirect("profile");
                }
                else
                {
                    lblMsgStatus.Text = "<b style='color:green'>Error...!!! Message not sent</b>";

                }
            }
        }
        else if(Session["user_from"] != null)
        {
            object username = opr.GetUsername(Session["user"].ToString());
            if (username != null)
            {
                info.username = username.ToString();
                
                info.message = TextBox1.Text;
                
                
                info.uid = Session["uid"].ToString();
                int i = opr.SendMessage(info,"reply");
                if (i > 0)
                {
                    lblMsgStatus.Text = "<b style='color:green'>Message sent successfully</b>";
                    //RedirectTo.Redirect("profile");
                }
                else
                {
                    lblMsgStatus.Text = "<b style='color:green'>Error...!!! Message not sent</b>";

                }
                Session.Remove("user_from");

            }
        }
    }
}