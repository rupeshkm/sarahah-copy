﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using BAL;
using PAL;
using System.Web.UI.WebControls;
using System.Diagnostics;

public partial class Master_Welcome_Default2 : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();

    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
            return;
        }
        GetMsg();
       
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtbox = row.FindControl("Txtbox1") as TextBox;

                string[] lines = txtbox.Text.Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries); int count = lines.Length;
                txtbox.Rows = count + 3;
            }
        }
    }

    public void GetMsg()
    {
        info.email = Session["user"].ToString();
        info.username = opr.GetUsername(info.email).ToString();
        string msg = Request.QueryString["reply"];
        string str = "";
        if(msg == "recieved")
        {
            str = "rcv";
        }
        else
        {
            str = "sent";
        }
        SqlDataReader dr = opr.GetMessages(info, str,"reply");
        GridView1.DataSource = dr;
        GridView1.DataBind();
        
    }


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                //ImageButton imgBtn = row.FindControl("ImageButton1") as ImageButton;
                //string userFrom = opr.SendReply(imgBtn.AlternateText);

                //Session["user_from"] = userFrom;
                //RedirectTo.Redirect("newmsg");

                ImageButton imgBtn = row.FindControl("ImageButton1") as ImageButton;
                string userFrom = opr.SendReply(imgBtn.AlternateText);
                Label lblRefNo = row.FindControl("Label3") as Label;
                string ref_no = lblRefNo.Text;
                info.uid = ref_no;
                Debug.WriteLine(userFrom + "ession");
                Session["user_from"] = userFrom;
                RedirectTo.Redirect("newmsg", "id=" + ref_no);
            }
        }
    }
}