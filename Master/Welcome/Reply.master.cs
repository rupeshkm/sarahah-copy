﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Welcome_Reply : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }

        
        if (Request.QueryString["reply"] == "recieved")
        {
            //lBtnMsgRcv.BackColor = System.Drawing.Color.AliceBlue;
            lBtnMsgRcv.Font.Underline = true;
            lBtnMsgRcv.ForeColor = System.Drawing.Color.Blue;
            lBtnMsgRcv.CssClass = "form-control panel-heading panel-primary";
            lBtnMsgRcv.Focus();
        }
        else if (Request.QueryString["reply"] == "sent")
        {
            lBtnSent.Font.Underline = true;
            lBtnSent.ForeColor = System.Drawing.Color.Blue;
            lBtnSent.CssClass = "form-control panel-heading panel-primary";
            lBtnSent.Focus();
        }
    }

    protected void lBtnMsgRcv_Click(object sender, EventArgs e)
    {


        Response.Redirect("~/Master/Welcome/Replies.aspx?reply=recieved");
    }

    protected void lBtnFav_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("fav");
    }

    protected void lBtnSent_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Welcome/Replies.aspx?reply=sent");
        //RedirectTo.Redirect("sent");
    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("newmsg");
    }
}
