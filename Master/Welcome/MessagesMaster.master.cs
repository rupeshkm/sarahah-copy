﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Welcome_MessagesMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }
        

        if (Request.Url.AbsoluteUri.Contains("Messages.aspx"))
        {
            //lBtnMsgRcv.BackColor = System.Drawing.Color.AliceBlue;
            lBtnMsgRcv.Font.Underline = true;
            lBtnMsgRcv.ForeColor = System.Drawing.Color.Blue;
            lBtnMsgRcv.CssClass = "form-control panel-heading panel-primary";
            lBtnMsgRcv.Focus();
        }
        else
        {
            lBtnSent.Font.Underline = true;
            lBtnSent.ForeColor = System.Drawing.Color.Blue;
            lBtnSent.CssClass = "form-control panel-heading panel-primary";
            lBtnSent.Focus();
        }
        if (Request.QueryString["reply"] != null)
        {
            head.Text = "Replies";
            Session["page"] = "reply";
        }
        
    }


    protected void lBtnMsgRcv_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("messages");
        
    }

    protected void lBtnFav_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("fav");
    }

    protected void lBtnSent_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("sent");
    }

    protected void Unnamed_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("newmsg");
    }
}
