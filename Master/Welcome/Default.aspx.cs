﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PAL;
using BAL;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

public partial class Master_Welcome_Default : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }
        GetMsg();
    }

    public void GetMsg()
    {

        //w.LoadControl("~/WebUserControl2.ascx");
        //Label Label1 = (Label)WebUserControl2.FindControl("Label1");
        //Label1.Text =   "";


        info.email = "Rupeshkumar@gmail.com";
        info.username = opr.GetUsername(info.email).ToString();
        SqlDataReader dr = opr.GetMessages(info, "sent");
        GridView1.DataSource = dr;
        GridView1.DataBind();



        //string data = "";
        //int i = 0;
        //while (dr.Read())
        //{
        //    string rcv = dr["user_to"].ToString();
        //    //string rcv1 = ConvertUrlsToLinks(rcv);
        //    string Message = dr["msg"].ToString();
        //    string time = dr["time"].ToString();
        //    data += "<tr><td><br/>" + Message + "<br/><br/></td><td class=" + "text-right" + "><br/>" + time + "<br/><b>" + rcv + "</b><br/></td></tr>";

        //}
        //return data;
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}