﻿using BAL;
using PAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Welcome_Default2 : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }

        info.searchName = Session["SearchUser"].ToString();

        
        SqlDataReader dr = opr.GetUsers(info);
        GridView1.DataSource = dr;
       
        GridView1.DataBind();

       

    }

    protected void lBtnUsername_Click(object sender, EventArgs e)
    {
        LinkButton btn = sender as LinkButton;
        string LBQ = btn.Text;

        Session["username"] = LBQ;

        //HttpCookie cookie = new HttpCookie("username");
        //cookie.Value = LBQ;
        RedirectTo.Redirect("newmsg");
    }
}
