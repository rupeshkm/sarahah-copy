﻿using BAL;
using MySql.Data.MySqlClient;
using PAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Welcome_Default : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    SecureText secure = new SecureText();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
            return;
        }
        GetMsg();
        System.Threading.Thread.Sleep(400);

        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtbox = row.FindControl("Txtbox1") as TextBox;
                
                string[] lines = txtbox.Text.Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries); int count = lines.Length;
                txtbox.Rows = count + 3;
            }
        }
    }

    public void GetMsg()
    {
        info.email = Session["user"].ToString();
        info.username = opr.GetUsername(info.email).ToString();
        SqlDataReader dr = opr.GetMessages(info,"rcv");
        GridView1.DataSource = dr;
        GridView1.DataBind();
       
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgBtn = row.FindControl("ImageButton1") as ImageButton;
                string userFrom = opr.SendReply(imgBtn.AlternateText);
                Label lblRefNo = row.FindControl("Label3") as Label;
                string ref_no = lblRefNo.Text;
                info.uid = ref_no;
             
                
                Session["user_from"] = userFrom;
                Session["uid"] = ref_no;
                RedirectTo.Redirect("newmsg","id=" + ref_no);
            }
        }
    }
}    