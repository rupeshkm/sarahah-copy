﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/MessagesMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Messages.aspx.cs" Inherits="Master_Welcome_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <style type="text/css">
        @media screen and (max-width: 1000px) and (min-width: 0px) {
            #lab {
                /*max-width:80%;
                overflow:hidden;
                overflow-wrap:break-word;
                position:absolute;*/
                margin:0px;
                padding:0px;

            }
            
        }
        
    </style>
<%--    <script type="text/javascript">
        function countRows() {
           document.getElementById("<%=GridView1.FindFieldTemplate(%>")
        }
    </script>--%>
    <div class="container">

        <div class="col-lg-12">
            
         <%--   <table class="table table-hover panel panel-success panel-heading table-responsive" style="font-family:Consolas;">
                <thead class="col-lg-12 text-center panel-heading">
                    <tr class="text-center">
                        <th class="col-lg-9 col-md-7 col-sm-6 text-center"> Message </th>
                        <th class="col-lg-3 col-md-5 col-sm-6 text-right"> Time </th>
                    </tr>
                </thead>
                <tbody class="panel-body" style="font-family:Consolas">
                    
                      
                        
                   
                    
                   
                </tbody>
            </table>
            --%>

            <asp:GridView ID="GridView1" runat="server" Width="100%" GridLines="none" BorderStyle="None" AutoGenerateColumns="false" Font-Names="Consolas">
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-lg-12 col-sm-12 text-center" SortExpression="DefaultValue">
                        <ItemTemplate>
                            <div id="presentation">

                                <div class="well text-center" style="background-color: white;">
                                    <div id="lab">
                                           <asp:TextBox ID="Txtbox1" runat="server" CssClass="form-control visible-sm visible-xs" ForeColor="Black" Enabled="false" BorderWidth="0" BackColor="White" Text='<%# Bind("msg") %>'  TextMode="MultiLine"></asp:TextBox>
                                            
                                    </div>
                                    <asp:Label ID="Label2" CssClass="text-primary visible-lg visible-md" runat="server" Text='<%#  Bind("msg") %>' Enabled="false" />
                                           
                                    <br />
                                    <br />

                                    <div class="row text-center">
                                     <div class="col-lg-4">
                                         <asp:Label ID="Label1"  CssClass="text-danger" runat="server"  Text='<%# Bind("time") %>' Enabled="true" />
                                 
                                     </div>
                                        <div class="col-lg-4">
                                         <asp:Label ID="Label3"  CssClass="text-danger" runat="server"  Text='<%# Bind("ref_no") %>' Enabled="true" />
                                 
                                     </div>

                                         <div class="col-lg-4">
                                             <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Reply" AlternateText='<%# Bind("uid") %>' OnClick="ImageButton1_Click" ImageUrl="../../assets/img/reply-right-arrow.png" Height="25" CssClass="img-responsive img-thumbnail" Width="25" BorderStyle="None" />
                                     </div>
                               
                                     </div>
                                 </div>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
    </div>
</asp:Content>

