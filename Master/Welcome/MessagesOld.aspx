﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/LoggedIn.master" AutoEventWireup="true" CodeFile="MessagesOld.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div class="container">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="active">
                <div class="panel panel-success">
                    <div class="panel-body">
                        <div class="text-center">
                            <span class="glyphicon glyphicon-inbox" style="font-size: x-large"></span>
                            <span class="text-center" style="font-size: x-large; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">Messages</span>
                        </div>
                        <br />
                        <br />

                        <div class="row">
                            <div class="col-lg-4 col-md-4 text-center">
                                <asp:LinkButton ID="lBtnMsgRcv" runat="server" CssClass="form-control alert-success" Font-Underline="false" Text="Received"  Font-Size="Small"></asp:LinkButton>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center" >
                                <asp:LinkButton ID="lBtnFav" runat="server" CssClass="form-control alert-success" Font-Underline="false" Text="Favorites"  Font-Size="Small"></asp:LinkButton>
                            </div>
                            <div class="col-lg-4 col-md-4 text-center">
                                <asp:LinkButton ID="lBtnSent" runat="server" CssClass="form-control alert-success" Font-Underline="false" Text="Sent" Font-Size="Small"></asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="container"></div>
            </div>
        </div>
        <div class="panel panel-danger"></div>
        
    </div>
    <div class="container">
        <div class="col-lg-12">
            <div class="well"></div>
        </div>
        
    </div>
  
</asp:Content>

