﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/LoggedIn.master" AutoEventWireup="true" CodeFile="SearchResults.aspx.cs" Inherits="Master_Welcome_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="container">

        <div class="col-lg-12">
            <asp:GridView ID="GridView1" runat="server" Width="100%" GridLines="none" BorderStyle="None" AutoGenerateColumns="false" Font-Names="Consolas">
                <Columns>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-lg-12 col-sm-12 text-center" SortExpression="DefaultValue">
                        <ItemTemplate>
                            <%--<div class="well text-right" style="margin-right: 20px">
                            <%--<div class="panel">--%>
                                <div class="row text-center">
                                    <div class="col-lg-6">
                                        <asp:Image ID="imgDP" runat="server" CssClass="img-circle img-responsive img-thumbnail" Width="100" Height="100" ImageUrl='<%# Bind("image")  %>' />
                                        <%--<img src="../../Users/rupesh.shutterbuzz.com/uploads/IMG_20170622_173336_576%20-%20Copy.JPG" />--%>
                                    </div>
                                    <%--<img src="../../assets/img/user.png" />--%>

                                    <div class="col-lg-6">
                                        <div class="row text-center">
                                            <asp:Label ID="Label2" CssClass="text-primary" runat="server" Text='<%#  Bind("name") %>' Enabled="false" />
                                    <br />
                                    <br />

                                    <div class="row text-center" style="margin-right: 10px">
  <asp:LinkButton ID="lBtnUsername" runat="server" CssClass="text-success" Text='<%# Bind("username") %>' ToolTip="Send Message" OnClick="lBtnUsername_Click" Enabled="true" />
                                 
                                    </div>
                                        </div>
                                    </div>
                                    
                                <%--</div>--%>
                            </div><br /><br />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>

