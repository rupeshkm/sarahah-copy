﻿using BAL;
using PAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

public partial class Default2 : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = "Active Users : " + Information.total_active.ToString();
        if(Session["user"] == null)
        {
            RedirectTo.Redirect("login");

        }
        LitFacebook.Text = "<a name=\"fb_share\" type=\"button\" share_url =\"http://kidstrail.inoday.co.in/\"></a>" +
                   "<script " +
                   "src=\"http://static.ak.fbcdn.net/connect.php/js/FB.Share\" " +
                   "type=\"text/javascript\"></script>";
        info.email = Session["user"].ToString();
        SqlDataReader dr = opr.GetData(info);

        
        object username = opr.GetUsername(Session["user"].ToString());
        if (username != null)
        {
            LblUserName.Text = username.ToString();
        }
        
        while (dr.Read())

            DP.ImageUrl = dr["image"] + "";
       
        if(DP.ImageUrl == "")
        {
            DP.ImageUrl = "~/assets/img/user.png";
        }

        info.username = username.ToString();
        info.receive = username.ToString();

        lblSnt.Text = opr.CountMsg(info,"sent") + "";
        lblRcvd.Text = opr.CountMsg(info,"rcv") + "";
        lblTotal.Text = opr.CountMsg(info,"rcv") + opr.CountMsg(info,"sent") + "";
    }

    protected void lBtn1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("messages");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("sent");
    }

    protected void LinkButton3_Click(object sender, EventArgs e)
    {

        RedirectTo.Redirect("newmsg");
    }
}