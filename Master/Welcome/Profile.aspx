﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/LoggedIn.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    
     <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10&appId=132844954019467";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <link href="../../css/floatingbtn.css" rel="stylesheet" />

    <div class="container">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row text-center">
<%--                <div class="panel">
                    <div class="panel-body">
                        <div class="img-responsive">--%>
                            <%--<img src="../../uploads/IMG_20170622_173336_576.JPG"  class="img-circle" height="100" width="100" />--%>
                            <asp:Image ID="DP" runat="server" CssClass="img-circle" ImageUrl = "~/assets/img/user.png" Height="100" Width="100" />
                            <%-- </div>
                    </div>
                </div>--%><br /><br />  
               <strong> <asp:Label ID="LblUserName" runat="server" Text="Username" CssClass="text-info"></asp:Label></strong><br />
               <div style="float:left; height:18px; margin-left:3px; overflow:hidden;"><asp:Literal ID="LitFacebook" runat="server" ></asp:Literal> </div>
                <%--<div class="fb-share-button" data-href="https://www.shutterbuzz.in" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.shutterbuzz.in%2F&amp;src=sdkpreparse">Share</a></div>--%>
                <a href="http://www.facebook.com/sharer/sharer.php?s=100&p[url]=
http://www.shutterbuzz.in">
                    <img src="../../assets/img/Facebook.png" onmouseover="Share" class="img-circle img-responsive center-block" height="30" width="30" /></a>
    <%--</asp:LinkButton>--%>
                
            </div>
            
        </div>
        
    </div><br /><br />

    <div class="container">

        <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="panel panel-success">
                <div class="panel-heading text-center" style="font-family: 'Times New Roman', Times, serif; font-size: xx-large">

                    <span class="glyphicon glyphicon-briefcase"></span>
                    Total Messages
                </div>
                <div class="panel-body">
                    <div class="text-center" style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: xx-large">
                       <asp:Label ID="lblTotal" runat ="server"><strong></strong></asp:Label>

                    </div>
                </div>
                <div class="panel-footer text-center">
                    <asp:LinkButton ID="lBtn1" runat="server" CssClass="btn btn-success" OnClick="lBtn1_Click"><span class="glyphicon glyphicon-new-window"></span></asp:LinkButton>
                </div>
            </div>
        </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="panel panel-warning">
                <div class="panel-heading text-center" style="font-family: 'Times New Roman', Times, serif; font-size: xx-large">

                    <span class="glyphicon glyphicon-inbox"></span>
                    Recieved
                </div>
                <div class="panel-body">
                    <div class="text-center" style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: xx-large">
                        <asp:Label ID="lblRcvd" runat ="server"><strong></strong></asp:Label>

                    </div>
                </div>
                <div class="panel-footer text-center">
                   <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-warning" OnClick="LinkButton1_Click"><span class="glyphicon glyphicon-new-window"></span></asp:LinkButton>
                
                </div>
            </div>
        </div>
           <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading text-center" style="font-family: 'Times New Roman', Times, serif; font-size: xx-large">

                    <span class="glyphicon glyphicon-send"></span>
                    Sent
                </div>
                <div class="panel-body">
                    <div class="text-center" style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; font-size: xx-large">
                        <asp:Label ID="lblSnt" runat ="server"><strong></strong></asp:Label>

                    </div>
                </div>
                <div class="panel-footer text-center">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-info" OnClick="LinkButton2_Click"><span class="glyphicon glyphicon-new-window"></span></asp:LinkButton>
                
                </div>
            </div>
        </div>
        
        
    </div>
    <div class="container">
        
        <div class="col-lg-12">
            <div class="text-center">
                <asp:LinkButton ID="LinkButton3" runat="server" Text="Send New Message" CssClass="btn btn-danger" OnClick="LinkButton3_Click"><span class="glyphicon glyphicon-new-window"><strong> Send New Message</strong></span></asp:LinkButton>
              <asp:Label ID="Label1" runat="server" CssClass="text-success pull-right" Visible="false" Text="Label"></asp:Label>
            </div>
            <br />
      
        </div>
    </div>
    <div id="container-floating">

  <%--<div class="nd5 nds" data-toggle="tooltip" data-placement="left" data-original-title="Simone"></div>
  <div class="nd4 nds" data-toggle="tooltip" data-placement="left" data-original-title="contract@gmail.com"><img class="reminder">
    <p class="letter">C</p>
  </div>
  <div class="nd3 nds" data-toggle="tooltip" data-placement="left" data-original-title="Reminder"><img class="reminder" src="//ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/1x/ic_reminders_speeddial_white_24dp.png"></div>
  --%><%--<div class="nd1 nds" data-toggle="tooltip" data-placement="left" data-original-title="Edoardo@live.it"><img class="reminder">
    <p class="glyphicon glyphicon-send"></p>
  </div>--%>

  <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" style=" width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;">
    <p class="plus">+</p>
    <asp:LinkButton runat="server" OnClick="LinkButton3_Click" class="edit" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/1x/bt_compose2_1x.png" />
  </div>

</div>
</asp:Content>

