﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Welcome/MessagesMaster.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Sent.aspx.cs" Inherits="Master_Welcome_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">

        <div class="col-lg-12">
            
            <%--<div class="table-responsive">--%>
                <%-- <asp:GridView ID="GridView1" runat="server" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="false" DataKeyNames="user_to" EmptyDataText="There are no data records to display.">  
                                        <Columns>  
                                            <asp:BoundField DataField="msg" HeaderText="Message" ReadOnly="True"  />  
                                            <asp:BoundField DataField="time" HeaderText="Time"  ItemStyle-CssClass="visible-xs" HeaderStyle-CssClass="visible-xs" />  
                                            <asp:BoundField DataField="user_to" HeaderText="To" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />  
                                            
                                            <asp:BoundField DataField="ContactTitle" HeaderText="ContactTitle" SortExpression="ContactTitle" HeaderStyle-CssClass="visible-md" ItemStyle-CssClass="visible-md" />  
                                            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" ItemStyle-CssClass="hidden-xs" HeaderStyle-CssClass="hidden-xs" />  
                                            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" ItemStyle-CssClass="hidden-xs" HeaderStyle-CssClass="hidden-xs" />  
                                            <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" HeaderStyle-CssClass="visible-md" ItemStyle-CssClass="visible-md" />  
                                            <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />  
                                            <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" HeaderStyle-CssClass="visible-md" ItemStyle-CssClass="visible-md" />  
                                            <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />  
                                        </Columns>  
                                    </asp:GridView>  --%>
            
             <asp:GridView ID="GridView1" runat="server" Width="100%" GridLines="none" BorderStyle="None" OnRowDataBound="GridView1_RowDataBound" AutoGenerateColumns="false" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Font-Names="Consolas">
                 <Columns>
                     

                     <%--<asp:BoundField DataField="msg"  HeaderText="Message" ItemStyle-Height="50px" HeaderStyle-CssClass="col-lg-10 text-center" ReadOnly="True" />--%>
                     <%--<asp:BoundField DataField="time" HeaderText="Time" HeaderStyle-CssClass="col-lg-2 text-center"  />--%>
                     <%--<asp:BoundField DataField="user_to" HeaderText="To" HeaderStyle-CssClass="col-lg-2 text-center" />--%>
                     <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-lg-12 col-sm-12 text-center" SortExpression="DefaultValue">
                         <ItemTemplate>
                            <div id="presentation">
                             <div class="well text-center" style="background-color:white;" >
                                  <%--<% //string str = Bind("msg"); %>--%>
                                  <asp:TextBox ID="txtbox1" runat="server" ForeColor="Black" CssClass="form-control visible-sm visible-xs" Enabled="false" BorderWidth="0" BackColor="White" Text='<%# Bind("msg") %>' Rows="10" TextMode="MultiLine"></asp:TextBox>
                                           
                                 <asp:Label ID="Label2" CssClass="text-primary visible-lg visible-md" runat="server" Text='<%#  Bind("msg") %>' Enabled="false" />
                                 <br />
                                 <br />
                                 <div class="row text-center">
                                     <div class="col-lg-6">
                                         <asp:Label ID="Label1"  CssClass="text-danger" runat="server" Text='<%# Bind("time") %>' Enabled="false" />
                                 
                                     </div>
                                     <div class="col-lg-6">
                                         <asp:LinkButton ID="LinkButton1" runat="server" CssClass="text-success" Text='<%# Bind("user_to") %>' OnClick="TextBox1_Click" Enabled="true" />
                                 
                                     </div>
                                 </div>
                                 <%--<asp:Label ID="txt_defaultValue_view" ForeColor="Tomato" runat="server" Text='<%# Bind("time") %>' Enabled="false" />
                                 <br />
                                 <asp:LinkButton ID="TextBox1" runat="server" CssClass="success" Text='<%# Bind("user_to") %>' OnClick="TextBox1_Click" Enabled="true" />
                                 <br />--%>
                                 </div> 
                             </div>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
               </div>
         
           <%-- <table class="table table-hover panel panel-success panel-heading table-responsive">
                <thead class="col-lg-12 text-center panel-heading" style="font-family:Consolas;">
                    <tr class="text-center">--%>
                        <%--<th class="col-lg-2 col-md-3 col-sm-4 text-center"> Recipient </th>--%>
                      <%--  <th class="col-lg-7 col-md-5 col-sm-4 text-center"> Message </th>
                        <th class="col-lg-3 col-md-5 col-sm-4 text-right"> Time </th>
                    </tr>
                </thead>
                <tbody class="panel-body" style="font-family:Consolas">
                    --%>
                   <%
//PAL.Information info = new PAL.Information();
//BAL.Operations opr = new BAL.Operations();
//info.email = Session["user"].ToString();
//info.username = opr.GetUsername(info.email).ToString();
//System.Data.SqlClient.SqlDataReader dr = opr.GetMessages(info, "sent");
////GridView1.DataSource = dr;
////GridView1.DataBind();
//string data = "";
//int i = 0;
//while (dr.Read())
//{
//    string rcv = dr["user_to"].ToString();

//    //string rcv1 = ConvertUrlsToLinks(rcv);
//    string Message = dr["msg"].ToString();
//    string time = dr["time"].ToString();
//    data = "<tr><td><br/>" + Message + "<br/><br/></td><td class=" + "text-right" + "><br/>" + time + "<br/><b><a href="+"\"NewMessage.aspx\">"+rcv+"</a></b><br/></td></tr>";
//    Response.Write(data);

//    // data += "<tr><td><br/>" + Message + "<br/><br/></td><td class=" + "text-right" + "><br/>" + time + "<br/><b></b><br/></td></tr>";

//}
                        %>
                      
                <%--        
                    
                   
                </tbody>
            </table>--%>
            
        </div>
    <%--</div>--%>
</asp:Content>

