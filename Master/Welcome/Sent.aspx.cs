﻿using BAL;
using MySql.Data.MySqlClient;
using PAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Welcome_Default : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            RedirectTo.Redirect("login");
        }
        GetMsg();


        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtbox = row.FindControl("Txtbox1") as TextBox;

                string[] lines = txtbox.Text.Split(new Char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries); int count = lines.Length;
                txtbox.Rows = count + 3;
            }

        }
    }

    public void GetMsg()
    {
        info.email = Session["user"].ToString();
        info.username = opr.GetUsername(info.email).ToString();
        SqlDataReader dr = opr.GetMessages(info, "sent");
        GridView1.DataSource = dr;
        GridView1.DataBind();
        //string data = "";
        //int i = 0;
        //while (dr.Read())
        //{
        //    string rcv = dr["user_to"].ToString();

        //    //string rcv1 = ConvertUrlsToLinks(rcv);
        //    string Message = dr["msg"].ToString();
        //    string time = dr["time"].ToString();
        //    data += "<tr><td><br/>" + Message + "<br/><br/></td><td class=" + "text-right" + "><br/>" + time + "<br/><b><a href="+"\"NewMessage.aspx\">"+rcv+"</a></b><br/></td></tr>";

        //   // data += "<tr><td><br/>" + Message + "<br/><br/></td><td class=" + "text-right" + "><br/>" + time + "<br/><b></b><br/></td></tr>";

        //}
        //return data;
    }
    //private string ConvertUrlsToLinks(string msg)
    //{
    //    string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
    //    Regex r = new Regex(regex, RegexOptions.IgnoreCase);
    //    return r.Replace(msg, "<a href=\"$1\" title=\"Click to open in a new window or tab\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
    //}

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int index = GridView1.SelectedRow.RowIndex;
        //string name = GridView1.SelectedRow.Cells[0].Text;
        //string country = GridView1.SelectedRow.Cells[1].Text;
        //string message = "Row Index: " + index + "\\nName: " + name + "\\nCountry: " + country;
        //Debug.WriteLine("message" + message);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
        //    e.Row.Attributes["style"] = "cursor:pointer";
        //}
    }



    protected void TextBox1_Click(object sender, EventArgs e)
    {
        LinkButton btn = sender as LinkButton;
        string LBQ = btn.Text;
       
        Session["username"] = LBQ;

        //HttpCookie cookie = new HttpCookie("username");
        //cookie.Value = LBQ;
        RedirectTo.Redirect("newmsg");
    }
}
