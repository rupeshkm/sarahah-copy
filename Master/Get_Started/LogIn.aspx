﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Get_Started/MasterPage.master" AutoEventWireup="true" CodeFile="LogIn.aspx.cs" Inherits="Master_Get_Started_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        function Validate() {
            if(document.getElementById("<%=txtUsername.ClientID%>").value == "" ||
                document.getElementById("<%=TxtPassword.ClientID%>").value == "") {
                //alert("Please Enter User Credentials");
                window.alert("Please Enter User Credentials");

                return false;
            }
            return true;
        }
    </script>
      <div class="container" style="margin-top: 50px">
            <div class="panel panel-warning" style="box-shadow:10px 10px 5px grey">
                <div class="panel-heading text-center" style="font-size:x-large;">Login to your account</div>
                <div class="panel-body" >
                    
                  

                </div><br />
                    <%-- row username --%>
                     <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" CssClass="form-control" ></asp:TextBox>

                    </div>
                    <div class="col-lg-3">
                        <asp:Label runat="server" id="lblEmail" style="color:red;font-size:small" Text=""></asp:Label>
                    </div>
                    

                </div><br />

                    <%-- row password --%>
                     <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="TxtPassword" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password"></asp:TextBox>

                    </div>
                         <div class="col-lg-3">
                        <asp:Label runat="server" id="lblPwd" style="color:red;font-size:small" Text=""></asp:Label>
                    </div>

                
            </div><br />
                <div class="panel-footer" style="background-color: white">
                    <div class="row">
                        <div class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3">

                            <asp:Button ID="btnLogin" CssClass="btn btn-success" OnClientClick="return Validate()" runat="server" Text="Log In" OnClick="btnLogin_Click"  />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">

                            <asp:Button ID="btnForgot" CssClass="btn btn-danger"  runat="server" Text="Forgot Password" OnClick="btnForgot_Click"   />
                        </div>

                    </div>
                </div>
            </div>
            
       </div>
</asp:Content>

