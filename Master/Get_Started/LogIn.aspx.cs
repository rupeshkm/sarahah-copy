﻿using BAL;
using DAL;
using PAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Get_Started_Default2 : System.Web.UI.Page
{
    DbConnection con = new DbConnection();
    Information info = new Information();
    Operations opr = new Operations();
    SystemInfo SI = new SystemInfo();


    protected void Page_Load(object sender, EventArgs e)
    {
        //Debug.WriteLine("SessionUser " + Session["user"]);

        if (Session["user"] != null)
        {
            RedirectTo.Redirect("profile");
            return;
        }

        


    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(txtUsername.Text))
        {
            lblEmail.ForeColor = System.Drawing.Color.Red;
            lblEmail.Text = "Please Enter Email";
            return;
        }
        else if (String.IsNullOrEmpty(TxtPassword.Text))
        {
            lblEmail.Text = "";
            lblPwd.ForeColor = System.Drawing.Color.Red;
            lblPwd.Text = "Please Enter Password";
            return;
        }
        //con.getCon();

        info.email = txtUsername.Text;
        Debug.WriteLine("email at login " + info.email);
        info.password = (TxtPassword.Text);
        Debug.WriteLine("pwd at login " + info.password);
        object o = opr.CheckEmail(info,"email");

        if (o == null)
        {
            lblPwd.Visible = false;
            lblPwd.Text = "";
            lblEmail.Visible = true;
            lblEmail.Text = "Email not exists !";

        }
        else
        {
            lblEmail.Visible = false;
            lblEmail.Text = "";
            o = opr.Login(info);
            if (o == null)
            {
                lblPwd.Visible = true;
                lblPwd.Text = "Incorrect Password !";

            }
            else
            {
                lblPwd.Visible = false;
                lblPwd.Text = "";
                Session["user"] = txtUsername.Text;
                //Information.total_active++;
                RedirectTo.Redirect("profile");
                //Response.Redirect("profile.aspx"); 
            }
        }
    }

    protected void btnForgot_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("forgot_password");
    }
}