﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Get_Started/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Master_Get_Started_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script type="text/javascript">
       function Validate() {
           if (document.getElementById("<%=txtFName.ClientID%>").value == "" ||
               document.getElementById("<%=txtLName.ClientID%>").value == "" ||
               document.getElementById("<%=txtEmail.ClientID%>").value == "" ||
                document.getElementById("<%=txtUsername.ClientID%>").value == "" ||
                document.getElementById("<%=TxtPassword.ClientID%>").value == "" ||
                document.getElementById("<%=txtConfirm.ClientID%>").value == "") {
               
               alert("* All fields are mandatory");
               return false;
           }

           if (document.getElementById("<%=TxtPassword.ClientID%>").value != document.getElementById("<%=txtConfirm.ClientID%>").value)
            {

               alert("* Password do not match");
               return false;
           }
           var user = document.getElementById("<%=txtUsername.ClientID%>").value;
           if (user.length < 6) {
               alert("Username must be atleast 6 characters long");
               return false;
           }
           
           var pwd = document.getElementById("<%=TxtPassword.ClientID%>").value;
           if (pwd.length < 6) {
               alert("Password must be atleast 6 characters long");
               return false;
           }
           
           
           return true;
       }
   </script>
    <div class="container" style="margin-top: 50px">
            <div class="panel panel-warning" style="box-shadow:10px 10px 5px grey">
                <div class="panel-heading text-center" style="font-size:x-large;">Get Register</div>
                <div class="panel-body" >
                    <div class="row">

                    <div class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3">
                        <asp:TextBox ID="txtFName" runat="server" placeholder="First Name" CssClass="form-control" OnTextChanged="txtFName_TextChanged"></asp:TextBox>
                    </div>
                       
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <asp:TextBox ID="txtLName" runat="server" placeholder="Last Name" CssClass="form-control"></asp:TextBox>
                    </div>
                      
                </div><br />
                    
                    <%-- email row --%>
                    <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" CssClass="form-control" TextMode="Email"></asp:TextBox>

                    </div>
                        <div class="col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label></div>

                </div><br />
                    <%-- row username --%>
                     <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" CssClass="form-control"></asp:TextBox>

                    </div>

                </div><br />

                    <%-- row password --%>
                     <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="TxtPassword" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password"></asp:TextBox>

                    </div>

                </div><br />

                     <%-- row confirm password --%>
                     <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="txtConfirm" runat="server" placeholder="Confirm Password" CssClass="form-control" TextMode="Password"></asp:TextBox>

                    </div>
                        </div><br />

            </div>
                <div class="panel-footer" style="background-color:white">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                            
                    <asp:Button ID="Button1" CssClass="btn btn-danger" runat="server" OnClientClick="return Validate()" Text="Sign Up" OnClick="Button1_Click"  />
                        </div>
                    </div>
                </div>
                </div>
            
       </div>
</asp:Content>

