﻿using System;
using PAL;
using BAL;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Net.Sockets;
using System.Text;

public partial class Master_Get_Started_Default : System.Web.UI.Page
{
    SystemInfo SI = new SystemInfo();
    //SystemOperations SO = new SystemOperations();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["user"] != null)
        {
            RedirectTo.Redirect("profile");
        }
        string folderPath = Server.MapPath("~/Users/");

        //Check whether Directory (Folder) exists.
        if (!Directory.Exists(folderPath))
        {
            //If Directory (Folder) does not exists. Create it.
            Directory.CreateDirectory(folderPath);
        }

    }
    public void GetData()
    {
        
    }

    protected void txtFName_TextChanged(object sender, EventArgs e)
    {

    }
    Information info = new Information();
    Operations opr = new Operations();

    
    protected void Button1_Click(object sender, EventArgs e)
    {
        info.firstname = txtFName.Text;
        info.lastname = txtLName.Text;
        info.email = txtEmail.Text;
        info.username = txtUsername.Text;
        info.password = (TxtPassword.Text);
        info.name = info.firstname + " " + info.lastname;
        if(info.password == txtConfirm.Text  && info.password != null)
        {
            Debug.WriteLine("password : " + info.password + " Confirm : " + txtConfirm.Text);
            object o = opr.CheckEmail(info, "email");
        
            object user = opr.CheckEmail(info, "user");
            if (o != null)
            {
                Label1.Visible = true;
                Label1.Text = "Email Already Exists";
                Label1.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else if (user != null)
            {
                Label1.Visible = true;
                Label1.Text = "Username not available";
                Label1.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else
            {

                Label1.Visible = false;
                Label1.Text = "";


            }

            if (info.email.Contains("@gmail.com"))
            {
                TcpClient tClient = new TcpClient("gmail-smtp-in.l.google.com", 25);
                string CRLF = "\r\n";
                byte[] dataBuffer;
                string ResponseString;
                NetworkStream netStream = tClient.GetStream();
                StreamReader reader = new StreamReader(netStream);
                ResponseString = reader.ReadLine();
                /* Perform HELO to SMTP Server and get Response */
                dataBuffer = BytesFromString("HELO KirtanHere" + CRLF);
                netStream.Write(dataBuffer, 0, dataBuffer.Length);
                ResponseString = reader.ReadLine();
                dataBuffer = BytesFromString("MAIL FROM:<rupeshkumar9312@gmail.com>" + CRLF);
                netStream.Write(dataBuffer, 0, dataBuffer.Length);
                ResponseString = reader.ReadLine();
                /* Read Response of the RCPT TO Message to know from google if it exist or not */
                dataBuffer = BytesFromString("RCPT TO:<" + info.email.Trim() + ">" + CRLF);
                netStream.Write(dataBuffer, 0, dataBuffer.Length);
                ResponseString = reader.ReadLine();
                Debug.WriteLine("Response String" + ResponseString);
                if (GetResponseCode(ResponseString) == 550)
                {
                    //Response.Write("<br/><br/>");
                    Label1.Visible = true;
                    Label1.Text = ("<B><font color='red'>Mail Address Does not Exist !</font></b>");
                    tClient.Close();
                    return;
                }
                /* QUITE CONNECTION */
                dataBuffer = BytesFromString("QUITE" + CRLF);
                netStream.Write(dataBuffer, 0, dataBuffer.Length);
                tClient.Close();
            }

            int i = opr.insertData(info);
            if (i > 0)
            {
                //string NewDir = Server.MapPath("~/Users/" + txtUsername.Text +"/");
                //if (!Directory.Exists(NewDir))
                //{
                //    Directory.CreateDirectory(NewDir);
                //}
                //opr.SendVerificationMail(info);
                //opr.SendActivationEmail(info.email);
                RedirectTo.Redirect("login");
            }
        }
        else
        {
            //LblPwd.Text = "Password don't match";
            //return;
        }

        //TcpClient tClient = new TcpClient("gmail-smtp-in.l.google.com", 25);
        
    }
    private byte[] BytesFromString(string str)
    {
        return Encoding.ASCII.GetBytes(str);
    }
    private int GetResponseCode(string ResponseString)
    {
        return int.Parse(ResponseString.Substring(0, 3));
    }

}

   
