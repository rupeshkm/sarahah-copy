﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Get_Started/MasterPage.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="Master_Get_Started_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container" style="margin-top: 50px">
            <div class="panel panel-warning" style="box-shadow:10px 10px 5px grey">
                <div class="panel-heading text-center" style="font-size:x-large;">Forgot Password</div>
                <div class="panel-body" >
                    
                    
                    <%-- email row --%>
                    <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" CssClass="form-control" TextMode="Email"></asp:TextBox>

                    </div>
                        <div class="col-lg-3">
                            <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label></div>

                </div><br />
                  

                 

            </div>
                <div class="panel-footer" style="background-color:white">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                            
                    <asp:Button ID="Button1" CssClass="btn btn-danger" runat="server" Text="Submit" OnClick="Button1_Click" />
                        </div>
                    </div>
                </div>
                </div>
            
       </div>
    
</asp:Content>

