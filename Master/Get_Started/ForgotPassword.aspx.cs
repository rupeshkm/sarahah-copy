﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PAL;
using BAL;
using DAL;

public partial class Master_Get_Started_Default2 : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void GetData()
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        bool success = false;
        info.email = txtEmail.Text;
        object o = opr.CheckEmail(info,"email");
        if (o != null)
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Green;

            success = opr.SendPasswordMail(txtEmail.Text, o.ToString());
            if (success)
            {
                Label1.Text = "<b style='color:green'>Password has been sent successfully via email !!!</b>";

            }
            else
            {
                Label1.Text = "<b style='color:green'Please Check your Internet Connection !!!</b>";
            }
        }
        else
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Red;
            Label1.Text = "Email not exists";
        }
    }
}