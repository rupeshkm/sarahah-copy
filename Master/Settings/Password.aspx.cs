﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PAL;
using BAL;
using DAL;

public partial class Master_Settings_Default2 : System.Web.UI.Page
{
    Information info = new Information();
    Operations opr = new Operations();
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }
   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        info.email = Session["user"].ToString();
        info.tempPassword = txtCurrent.Text;
        info.newPassword = txtNew.Text;
        if(info.newPassword == txtConfirm.Text)
        {
            int i = opr.UpdatePasword(info);
            if(i > 0)
            {
                LblStatus.Text = "<b style='color:green'>Successfully Updated</b>";
            }
            else
            {
                LblStatus.Text = "<b style='color:Red'>Update Failed !</b>";
            }
        }
        else
            LblStatus.Text = "<b style='color:red'>Password Mismatch</b>";

    }
}