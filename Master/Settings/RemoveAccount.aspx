﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Settings/MasterPage.master" AutoEventWireup="true" CodeFile="RemoveAccount.aspx.cs" Inherits="Master_Settings_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-lg-9 col-md-9 col-sm-9">
        <div class="panel panel-default" style="visibility: visible">
            <div class="panel-heading"><b>Remove Account</b></div>
            <div class="panel-body" style="margin: 40px">
                <div class="alert alert-danger" style="font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">
                    <br /><b>Are you sure that you want to delete your account?<br />
                        Deleting the account is irreversible!</b>
                    <br />
                    <br />
                </div>
            </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-lg-2 col-lg-offset-4 col-md-6 col-sm-12">
                    <span>
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Remove" /></span>
                </div>
                <div class="col-lg-2  col-md-6 col-sm-12">
                    <input id="Reset1" type="button" class="btn btn-default" value="Cancel" />
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>

