﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Settings_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lBtnMsg_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Welcome/Messages.aspx");
    }

    protected void lBtnUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Master/Welcome/Profile.aspx");
    }

    protected void lBtnSetting_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("settings");    
    }

    protected void lBtnPersonal_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("settings");
    }

    protected void lBtnPwd_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("password");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("remove");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("messages");
    }
}
