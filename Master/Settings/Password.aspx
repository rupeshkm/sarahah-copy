﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Settings/MasterPage.master" AutoEventWireup="true" CodeFile="Password.aspx.cs" Inherits="Master_Settings_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="col-lg-9 col-md-9 col-sm-9">

                <div class="panel panel-warning" style="visibility: visible">
                    <div class="panel-heading"><b>Change Password</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col col-sm-12 text-center" style="margin-top: 6px">
                                <b>Current Password</b>

                            </div>
                            <div class="col-lg-9 col-md-6 col col-sm-12">
                                <asp:TextBox ID="txtCurrent" runat="server" TextMode ="Password" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-lg-3 col-md-6 col col-sm-12 text-center" style="margin-top: 6px">
                                <b>New Password</b>

                            </div>
                            <div class="col-lg-9 col-md-6 col col-sm-12">
                                <asp:TextBox ID="txtNew" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-lg-3 col-md-6 col col-sm-12 text-center" style="margin-top: 6px">
                                <b>Confirm Password</b>

                            </div>
                            <div class="col-lg-9 col-md-6 col col-sm-12">
                                <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />



                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-2 col-lg-offset-4 col-md-6 col-sm-12">
                                <span>
                                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Save" OnClick="btnSave_Click" /></span>
                            </div>
                            <div class="col-lg-2  col-md-6 col-sm-12">
                                <input id="Reset1" type="reset" class="btn btn-default" value="Cancel" />
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row text-center">
        
    <asp:label id="LblStatus" runat="server" text=""></asp:label>
    </div>

            </div>
</asp:Content>

