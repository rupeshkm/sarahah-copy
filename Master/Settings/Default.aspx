﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Settings/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Master_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="col-lg-9 col-md-9 col-sm-9">

                <div class="panel panel-warning" style="visibility: visible">
                    <div class="panel-heading"><b>Edit Personal Information</b></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-12 text-center" style="margin-top: 6px">
                                <b>Name</b>

                            </div>
                            <div class="col-lg-9 col-md-6 col col-sm-12">
                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-lg-3 col-md-6 col col-sm-12 text-center" style="margin-top: 6px">
                                <b>Email</b>
                            </div>
                            <div class="col-lg-9 col-md-6 col col-sm-12">
                                <asp:TextBox ID="txtEmail" runat="server" TextMode="Email" CssClass="form-control" OnTextChanged="txtEmail_TextChanged"></asp:TextBox>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-lg-3 col-md-6 col col-sm-12 text-center" style="margin-top: 6px">
                                <b>Change Image</b>

                            </div>
                            <div class="col-lg-5 col-md-6 col col-sm-12">
                                <asp:FileUpload ID="FileUpload1" runat="server" accept=".png,.jpg,.jpeg,.gif" CssClass="form-control" BorderStyle="None" />
                            </div>
                        </div>
                        
                        <br />
                        <div class="row">
                        <div class="col-lg-5 col-lg-offset-5 col-md-6 col col-sm-12" style="align-items:center">
                                <asp:image ID="img" imageUrl="~/uploads/uc-jug1sgdu.jpg" runat="server" cssClass="img-responsive" width="100px" height="100px" style="position:center"></asp:image>
                            </div>
                           
                        </div>


                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-2 col-lg-offset-4 col-md-6 col-sm-12">
                                <span>
                                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Save" OnClick="btnSave_Click" /></span>
                            </div>
                            <div class="col-lg-2  col-md-6 col-sm-12">
                                <input id="Reset1" type="reset" class="btn btn-default" value="Cancel" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div class="row text-center">
        
    <asp:label id="LblStatus" runat="server" text=""></asp:label>
    </div>

       

       
         
</asp:Content>

