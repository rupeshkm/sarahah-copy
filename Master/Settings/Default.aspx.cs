﻿using BAL;
using MySql.Data.MySqlClient;
using PAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetData();
        }
        //GetData();
    }

    Operations opr = new Operations();
    Information info = new Information();

    public void GetData()
    {
        info.email = Session["user"].ToString();
        //txtName..email;
        SqlDataReader o = opr.GetData(info);
        if (o.HasRows)
        {
            while (o.Read())
            {
                txtName.Text = o["name"] + "" ;
                txtEmail.Text = Session["user"] + "";
                img.ImageUrl = o["image"] + "";
                //img.ImageUrl = Server.MapPath(o["image"] + "");
                //LblStatus.Text = o["image"] + "";
                
            }

        }
       
    }

    string tt = "";

    protected void btnSave_Click(object sender, EventArgs e)
    {
        info.name = txtName.Text;
        info.email = Session["user"].ToString();
        //string tempname = txtName.Text;
        info.tempEmail = txtEmail.Text;
        
        FileUpload();
        //if(!FileUpload())
        //{
        //    LblStatus.Text = "<b style='color:Red'>Please use only .jgp , .png , .jpeg , .gif file</b>";
        //    return;
        //}

        try
        {
            int i = opr.Update(info.tempEmail.ToString(), info.name, info);
            if (i > 0)
            {
                LblStatus.Text = "<b style='color:green'>Successfully Updated</b>";
            }

            else
                LblStatus.Text = "<b style='color:Red'>Please Try Again Later...!</b>";
            Session.RemoveAll();
            info.email = info.tempEmail;
            Session["user"] = info.email;
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            if (ex.ToString().Contains("Violation of PRIMARY KEY"))
            {
                LblStatus.Text = "<b style='color:red'>Email already exists</b>";
            }
        }
        //LblStatus.Text = LblStatus.Text + "<b style='color:Red'>" + Session["user"]+"</b>";
        GetData();


    }

    public void FileUpload()
    {
        object o = opr.GetUsername(Session["user"].ToString());
        if (o != null)
        {
            string username = o.ToString();
            string Dir = Server.MapPath("~/Users/" + username + "/uploads/");
            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }

            if (FileUpload1.HasFile)
            {

                string extension = System.IO.Path.GetExtension(FileUpload1.FileName);
                
                string str = FileUpload1.FileName;
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png" || extension.ToLower() == ".gif")
                {
                    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Users/" + username + "/uploads/" + str));
                    info.filename = "~/Users/" + username + "/uploads/" + str.ToString().ToLower();

                }
                else
                {
                    //LblStatus.Text = extension;
                    LblStatus.Text = "<b style='color:Red'>Please use only .jgp , .png , .jpeg , .gif file</b>";
                }
                Console.WriteLine(info.filename);


            }
            else
            {
                info.filename = img.ImageUrl;
            }
        }

        //if (FileUpload1.HasFile)
        //{
        //    string str = FileUpload1.FileName;
        //    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/uploads/" + str));
        //    info.filename = "~/uploads/" + str.ToString().ToLower();
        //}
        //else
        //{
        //    info.filename = img.ImageUrl;
        //}
    }

    protected void txtEmail_TextChanged(object sender, EventArgs e)
    {
        tt = txtName.Text;
    }
}