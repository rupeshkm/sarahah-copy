﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //RedirectTo.Redirect("login");
    }

    protected void BtnSrch_ServerClick(object sender, EventArgs e)
    {

    }

    protected void lblSignUp_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("signup");
    }

    protected void lblLogin_Click(object sender, EventArgs e)
    {
        RedirectTo.Redirect("login");
    }

    protected void ltbnFB_Click(object sender, EventArgs e)
    {
        Response.Redirect("https://www.facebook.com/shutterbuzz.in/");
    }
}